require('script!../../vendors/hcap.js');

var FormView = Backbone.View.extend({
    template: require('../templates/form.hbs'),
    el: "#app",

    render: function(){
        this.$el.html(this.template());

        
    }
});

export default FormView;