require('script!../../vendors/hcap.js');

var KBrowserView = Backbone.View.extend({
    template: require('../templates/kbrowser.hbs'),
    el: "#app",

    render: function () {

        var self = this;
        /*var Procentric = require('pelican-procentric');
        window.PelicanDevice = new Procentric();*/

        // hcap.time.setLocalTime(window.serverTime);

        $.getJSON('http://apollo.local:3030')
            .done(function (data) {
                console.log('we have time data');

               hcap.time.setLocalTime({
                    "year": data.year,
                    "month":data.month,
                    "day":data.day,
                    "hour":data.hour,
                    "minute":data.minute,
                    "second":data.second,
                    "gmtOffsetInMinute":data.gmtOffsetInMinute,
                    "isDaylightSaving":data.isDaylightSaving,
                    "onSuccess":function() {
                        console.log("TIME SET onSuccess");
                        console.dir(data);
                        hcap.time.getLocalTime({
                            "onSuccess":function(s) {
                                hcap.preloadedApplication.getPreloadedApplicationList({
                                    "onSuccess": (s) => {
                                        self.$el.html(self.template({
                                            apps: s.list,
                                            date: new Date()
                                        }));

                                        hcap.preloadedApplication.launchPreloadedApplication({
                                            "id": "144115188075855877", // Web Browser
                                            // "parameters": "{ 'target': 'http://healthy.kaiserpermanente.org' }",

                                            "parameters": "{ 'target': 'http://kp.org' }",

                                            // "parameters": "{ 'target': 'http://apollo.local:7000/#form' }",
                                            // "parameters": "{ 'target': 'http://google.com' }",
                                            // "parameters": "{ 'target': 'http://garrettsullins.com' }",
                                            // "parameters": "{ 'target': 'https://cbracco.github.io/html5-test-page/' }",

                                            "onSuccess": function () {
                                                console.log("onSuccess");
                                            },
                                            "onFailure": function (f) {
                                                console.log("onFailure : errorMessage = " + f.errorMessage);
                                            }
                                        });
                                    },
                                    "onFailure": (f) => {
                                        this.$el.html(this.template({
                                            error: f
                                        }));
                                    }
                                });
                                console.log("onSuccess : \n" +
                                    "TV localtime = " + s.year + "-" + s.month + "-" + s.day + " " + s.hour + ":" + s.minute + ":" + s.second + "\n" +
                                    "GMT offset = " + s.gmtOffsetInMinute + "\n" +
                                    "daylight saving = " + s.isDaylightSaving);
                            },
                            "onFailure":function(f) {
                                console.log("onFailure : errorMessage = " + f.errorMessage);
                            }
                        });
                    },
                    "onFailure":function(f) {
                        console.log("TIME SET onFailure : errorMessage = " + f.errorMessage);
                    }
                });


            })
            .fail(function (jqxhr, textStatus, error) {
                console.log("!!! TIME REQUEST FAILED !!!");
                console.log(error);
            });

    }
});

export default KBrowserView;