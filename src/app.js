import Router from './router';
import KBView from './views/kbrowser';

require('script!../vendors/hcap.js');
require("./less/bootswatch.cyborg.less");

window.App = {};

App.Router = new Router();

Backbone.history.start();