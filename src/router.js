import KBrowserView from './views/kbrowser';
import FormView from './views/form';

const Router = Backbone.Router.extend({

    routes: {
        "home": "home",
        "form": "form",
        "*main": "main"
    },

    main: function(){
        console.log('Route: Main');
        var kbv = new KBrowserView();
        kbv.render();
    },

    form: function(){
        console.log('Route: Form');
        var fv = new FormView();
        fv.render();
    }

});

export default Router;
