var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');

// help Date functions
Date.prototype.stdTimezoneOffset = function () {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.dst = function () {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}

// CORS
app.use(cors());

// be able to parse/use POST/GET data in requests
app.use( bodyParser.json() ); // support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // support URL-encoded bodies
    extended: true
}));

// Routes
app.get('/', function (req, res) {
    var d = new Date();
    var objTime = {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        hour: d.getHours(),
        minute: d.getMinutes(),
        second: d.getSeconds(),
        gmtOffsetInMinute: d.stdTimezoneOffset(),
        isDaylightSaving: d.dst()
    };

    var str = JSON.stringify(objTime);

    res.setHeader("Content-Length", str.length);
    res.setHeader('Content-Type', 'text/javascript');
    res.statusCode = 200;
    res.end(str.toString());
})

app.set('port', process.env.PORT || 3030);
var server = app.listen(app.get('port'), function () {
    console.log('Example app listening on port 3030!')
})