var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, '');

var config = {
    debug: true,
    devtool: 'source-map',

    context: APP_DIR,

    entry: [
        './src/app.js'
    ],

    devServer: {
        contentBase: path.join(__dirname, "src"),
        host: "0.0.0.0",
        port: "7000"
    },

    output: {
        filename: 'lg-kbrowser-test.js',
        path: BUILD_DIR,
    },

    module: {
        loaders: [
            {test: /\.json$/, loader: 'json'},
            {test: /\.js(x)?$/, exclude: /(node_modules|vendors)/, loaders: ['babel']},
            {test: /\.html$/, exclude: /node_modules/, loader: 'file?name=[name].[ext]'},
            {test: /\.(jpg|png|gif)$/, exclude: /node_modules/, loader: 'file?name=images/[name].[ext]'},
            {test: /\.css$/, loader: "style!css"},
            {test: /\.less$/, loader: "style!css!less"},
            {test: /\.hbs$/, loader: "handlebars"},
            {test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'}
        ]
    },

    // resolve helps resolving 'pelican' as if it's a npm module
    resolve: {
        root: [
            path.resolve('./')
        ],
        alias: {
            // Always resolve backbone to the backbone package instead of package's own dependency.
            // https://github.com/webpack/webpack/issues/1165
            'backbone': require.resolve('backbone')
        }
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            Backbone: "backbone"
        })
    ]

};

module.exports = config;